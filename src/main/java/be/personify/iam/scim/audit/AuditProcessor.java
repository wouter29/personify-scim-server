package be.personify.iam.scim.audit;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import be.personify.audit.AuditLoggerImplementations;
import be.personify.iam.scim.util.PropertiesUtil;
import be.personify.util.StringUtils;
import be.personify.util.audit.AuditLogger;
import be.personify.util.audit.Auditable;
import jakarta.annotation.PostConstruct;




@Aspect
@Component
public class AuditProcessor{
	
	private static final Logger logger = LogManager.getLogger(AuditProcessor.class.getName());
	
	private static List<AuditLogger> auditLoggers = new ArrayList<AuditLogger>();
	
	@Value("${auditing.enabled}")
	protected boolean auditingEnabled;
	
	@Value("${auditing.ignoreParameters}")
	protected String ignoreParameters;
	
	protected List<String> ignoreParameterList = null;
	
	@Autowired
	protected Environment env;

	
	
	/**
	 * Initializes the audit logger
	 */
	@PostConstruct
	public void init() {
		ignoreParameterList = Arrays.asList(ignoreParameters.split(StringUtils.COMMA));
		if ( auditingEnabled ) {
			for ( AuditLoggerImplementations impl : AuditLoggerImplementations.values() ) {
				String className = impl.getClassName();
				String enabledProperty = PropertiesUtil.getPropertiesFromEnv(env).getProperty("auditing." + impl.name() + ".enabled");
				if ( enabledProperty != null && enabledProperty.equalsIgnoreCase("true")) {
					try {
						Class<?> c = Class.forName(className);
						AuditLogger auditLogger = (AuditLogger)c.newInstance();
						auditLogger.configure(getAuditConfiguration("auditing." + impl.name()));
						auditLoggers.add(auditLogger);
						logger.info("successfully added audit logger of type {} and classname {}", impl.name(), className);
					}
					catch ( Exception e ) {
						logger.error("can not instantiate audit logger of type {} and classname {}", impl.name(), className, e);
					}
				}
				else {
					logger.info("not adding audit logger of type {} and classname {} because not enabled, set the following property to enable ->{}<-", impl.name(), className, "auditing." + impl.name() + ".enabled" );
				}
			}
		}
		else {
			logger.info("auditing not enabled, set following property to enable : {}", "auditing.enabled=true" );
		}
	}
	
	
	private Map<String,Object> getAuditConfiguration(String implementation ) {
    	Map<String,Object> configuration = new HashMap<String,Object>();
    	Properties props = PropertiesUtil.getPropertiesFromEnv(env);
    	
    	for( Entry<Object,Object> entry :  props.entrySet() ) {
    		String kkey = entry.getKey().toString();
    		if ( kkey.startsWith(implementation)) {
    			String key = kkey.substring(kkey.indexOf(implementation) + implementation.length() + 1, kkey.length());
        		logger.info("putting key {} to value {}", key, entry.getValue());
        		configuration.put(key,entry.getValue());
    		}
    	}
    	return configuration;
	}
	
	
	

    @AfterReturning(pointcut = "@annotation(be.personify.util.audit.Auditable)", returning = "object")
    private void log(JoinPoint joinPoint, Object object) throws IllegalAccessException {
    	if ( auditingEnabled && !auditLoggers.isEmpty() ) {
    		
            Method method = ((MethodSignature)joinPoint.getSignature()).getMethod();
            Auditable aspectLogger = method.getAnnotation(Auditable.class);
            
            
            if ( aspectLogger.enabled()) {
            	
            	//fill parameter map
                Parameter[] parameters = method.getParameters();
                Object[] signatureArgs = joinPoint.getArgs();
                Map<String,Object> paramMap = new HashMap<String, Object>();
                int i = 0;
                logger.info(" ignoreParameterList {} ", ignoreParameterList);
                logger.info(" ignoreParameterList size {} ", ignoreParameterList.size());
                for ( Parameter p : parameters) {
                	logger.info("Param " + p.getDeclaringExecutable().getName() + " - " + p.getName() + " - " + p.getType().getName());
                	if (!ignoreParameterList.contains(p.getType().getName())) {
                		logger.info("not contains");
                		paramMap.put(p.getName(), signatureArgs[i++]);
                	}
                }
                
            	
		        for ( AuditLogger logger : auditLoggers ) {
		        	
		        	String action = aspectLogger.action();
		        	if ( StringUtils.isEmpty(action) ) {
		        		action = method.getName();
		        	}
		        	
		        	String entity = aspectLogger.entity();
		        	if ( StringUtils.isEmpty(entity)) {
		        		entity = object.getClass().getName();
		        	}
		        	
		        	Map<String,Object> data = new HashMap<String, Object>();
		        	data.put("parameters", paramMap);
		        	data.put("returned", object);
		        	
		        	 
		        	//if loggers is defined only log to loggers specified
		        	if ( aspectLogger.loggers().length > 0 ) {
		        		if ( Arrays.asList(aspectLogger.loggers()).contains(logger.name())){
		        			logger.log(entity, action , null, data);
		        		}
		        	}
		        	else {
		        		logger.log(entity, action , null, data);
		        	}
		        }
            }
        }
    }

}